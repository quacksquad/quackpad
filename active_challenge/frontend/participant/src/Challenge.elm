module Challenge exposing (main)

import Browser
import Html exposing (Html, div, text, h1, h2, p, button, textarea, input, label)
import Html.Attributes exposing (style, placeholder, value, rows, cols, type_)
import Html.Events exposing (onClick, onInput)
import Time exposing (Posix, Zone, now, every, millisToPosix, toHour, toMinute, toSecond)
import Task exposing (perform)


main =
    Browser.element { init = init, update = update, view = view, subscriptions = subscriptions }


-- Model
type alias Model =
    { challengeName : String
    , problemStatement : String
    , currentTime : Posix
    , timeZone : Zone
    , solution : String
    , repoLink : String
    , showModal : Bool
    , showThankYou : Bool
    }


init : () -> (Model, Cmd Msg)
init _ =
    ( { challengeName = "Challenge Name"
      , problemStatement = "Problem Statement"
      , currentTime = millisToPosix 0
      , timeZone = Time.utc
      , solution = ""
      , repoLink = ""
      , showModal = False
      , showThankYou = False
      }
    , Task.perform ZoneUpdated Time.here
    )


-- Messages
type Msg
    = TimeUpdated Posix
    | ZoneUpdated Zone
    | Submit
    | UpdateChallengeName String
    | UpdateSolution String
    | UpdateRepoLink String
    | ConfirmSubmit
    | CancelSubmit


-- Update
update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        TimeUpdated newTime ->
            ( { model | currentTime = newTime }, Cmd.none )

        ZoneUpdated newZone ->
            ( { model | timeZone = newZone }, Cmd.none )

        Submit ->
            ( { model | showModal = True }, Cmd.none )

        ConfirmSubmit ->
            ( { model | showModal = False, showThankYou = True }, Cmd.none )

        CancelSubmit ->
            ( { model | showModal = False }, Cmd.none )

        UpdateChallengeName newName ->
            ( { model | challengeName = newName }, Cmd.none )

        UpdateSolution newSolution ->
            ( { model | solution = newSolution }, Cmd.none )

        UpdateRepoLink newLink ->
            ( { model | repoLink = newLink }, Cmd.none )


-- View
view : Model -> Html Msg
view model =
    if model.showThankYou then
        div
            [ style "height" "100vh"
            , style "background-color" "#f0f0f0"
            , style "background-image" "url('LB_bg.png')"
            , style "background-size" "cover"
            , style "background-repeat" "no-repeat"
            , style "background-position" "center"
            , style "position" "relative"
            , style "display" "flex"
            , style "flex-direction" "column"
            , style "align-items" "center"
            , style "justify-content" "center"
            ]
            [ h1
                [ style "margin" "0"
                , style "padding" "0"
                , style "color" "white"
                , style "font-size" "150px"
                , style "text-shadow" "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000"
                ]
                [ text "Thank You!" ]
            ]
    else
        div
            [ style "height" "100vh"
            , style "background-color" "#f0f0f0"
            , style "background-image" "url('LB_bg.png')"
            , style "background-size" "cover"
            , style "background-repeat" "no-repeat"
            , style "background-position" "center"
            , style "position" "relative"
            , style "display" "flex"
            , style "flex-direction" "column"
            , style "align-items" "center"
            , style "justify-content" "flex_start"
            ]
            [ h1
                [ style "margin" "0"
                , style "padding" "0"
                , style "color" "white"
                , style "font-size" "150px"
                , style "text-shadow" "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000"
                ]
                [ text "Active Challenges" ]
            , h2
                [ style "color" "white"
                , style "font-size" "45px"  -- Decreased by 10%
                , style "margin" "10px"
                , style "text-shadow" "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000"
                ]
                [ text model.challengeName ]
            , div
                [ style "display" "flex"
                , style "flex-direction" "column"
                , style "align-items" "flex-start"
                , style "gap" "20px"
                , style "width" "80%"
                , style "padding" "10px"
                ]
                [ p
                    [ style "color" "white"
                    , style "font-size" "26px"  -- Increased by 10%
                    , style "text-shadow" "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000"
                    , style "margin" "10px 0"
                    , style "text-align" "left"
                    , style "width" "100%"
                    ]
                    [ text model.problemStatement ]
                , div
                    [ style "display" "flex"
                    , style "flex-direction" "column"
                    , style "align-items" "flex-start"
                    , style "gap" "10px"
                    , style "width" "100%"
                    ]
                    [ label
                        [ style "color" "black"
                        , style "font-size" "45px"  -- Decreased by 10%
                        ]
                        [ text "Your Solution" ]
                    , textarea
                        [ placeholder "Paste your code here"
                        , value model.solution
                        , onInput UpdateSolution
                        , style "padding" "10px"
                        , style "font-size" "18px"
                        , style "border" "2px solid black"
                        , style "border-radius" "4px"
                        , rows 10
                        , cols 80
                        ]
                        []
                    , label
                        [ style "color" "black"
                        , style "font-size" "20px"
                        ]
                        [ text "GitLab/GitHub Link" ]
                    , input
                        [ placeholder "Enter your repo link"
                        , value model.repoLink
                        , onInput UpdateRepoLink
                        , style "padding" "10px"
                        , style "font-size" "18px"
                        , style "border" "2px solid black"
                        , style "border-radius" "4px"
                        , type_ "text"
                        ]
                        []
                    ]
                ]
            , div
                [ style "position" "absolute"
                , style "bottom" "30px"
                , style "left" "20px"
                , style "display" "flex"
                , style "align-items" "center"
                ]
                [ p
                    [ style "color" "black"
                    , style "font-size" "30px"
                    , style "margin" "0 10px 0 0"
                    ]
                    [ text (timeToString model.currentTime model.timeZone) ]
                ]
            , div
                [ style "position" "absolute"
                , style "bottom" "30px"
                , style "right" "20px"
                , style "display" "flex"
                , style "align-items" "center"
                ]
                [ button
                    [ onClick Submit
                    , style "background-color" "#fffa4a"
                    , style "padding" "20px 60px"
                    , style "font-size" "16px"
                    , style "font-family" "Chewy, cursive"
                    , style "border-radius" "10px"
                    ]
                    [ text "Submit" ]
                ]
            , if model.showModal then
                div
                    [ style "position" "fixed"
                    , style "top" "0"
                    , style "left" "0"
                    , style "width" "100%"
                    , style "height" "100%"
                    , style "background-color" "rgba(0, 0, 0, 0.5)"
                    , style "display" "flex"
                    , style "align-items" "center"
                    , style "justify-content" "center"
                    ]
                    [ div
                        [ style "background-color" "#add8e6"
                        , style "padding" "20px"
                        , style "border-radius" "10px"
                        , style "text-align" "center"
                        ]
                        [ p [ style "color" "black", style "font-size" "20px" ] [ text "Are you sure?" ]
                        , div
                            [ style "display" "flex", style "gap" "10px", style "justify-content" "center" ]
                            [ button
                                [ onClick ConfirmSubmit
                                , style "background-color" "#4CAF50"
                                , style "color" "white"
                                , style "padding" "10px 20px"
                                , style "border" "none"
                                , style "border-radius" "5px"
                                ]
                                [ text "Yes" ]
                            , button
                                [ onClick CancelSubmit
                                , style "background-color" "#f44336"
                                , style "color" "white"
                                , style "padding" "10px 20px"
                                , style "border" "none"
                                , style "border-radius" "5px"
                                ]
                                [ text "No" ]
                            ]
                        ]
                    ]
              else
                text ""
            ]


-- Subscriptions
subscriptions : Model -> Sub Msg
subscriptions _ =
    every 1000 TimeUpdated


-- Helper function to format the time
timeToString : Posix -> Zone -> String
timeToString posix zone =
    let
        hours = String.fromInt (toHour zone posix)
        minutes = String.fromInt (toMinute zone posix)
        seconds = String.fromInt (toSecond zone posix)
    in
    "Current Time: " ++ hours ++ ":" ++ minutes ++ ":" ++ seconds
