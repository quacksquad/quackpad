module Main exposing (main)

import Browser
import Html exposing (Html, div, text, h1, textarea, label, input, img)
import Html.Attributes exposing (style, placeholder, value, rows, cols, type_, src)
import Html.Events exposing (onInput)

main =
    Browser.sandbox { init = init, update = update, view = view }

-- MODEL

type alias Model =
    { challengeName : String
    , problemStatement : String
    , startDateTime : String
    , endDateTime : String
    }

init : Model
init =
    { challengeName = ""
    , problemStatement = ""
    , startDateTime = ""
    , endDateTime = ""
    }

-- UPDATE

type Msg
    = UpdateChallengeName String
    | UpdateProblemStatement String
    | UpdateStartDateTime String
    | UpdateEndDateTime String

update : Msg -> Model -> Model
update msg model =
    case msg of
        UpdateChallengeName newName ->
            { model | challengeName = newName }

        UpdateProblemStatement newStatement ->
            { model | problemStatement = newStatement }

        UpdateStartDateTime newStartDateTime ->
            { model | startDateTime = newStartDateTime }

        UpdateEndDateTime newEndDateTime ->
            { model | endDateTime = newEndDateTime }

-- VIEW

view : Model -> Html Msg
view model =
    div 
        [ style "height" "100vh"
        , style "background-color" "#f0f0f0"
        , style "background-image" "url('LB_bg.png')"
        , style "background-size" "cover"
        , style "background-repeat" "no-repeat"
        , style "background-position" "center"
        , style "position" "relative"
        , style "display" "flex"
        , style "flex-direction" "column"
        , style "align-items" "center"
        , style "justify-content" "flex_start"
        ] 
        [ h1 
            [ style "margin" "0"
            , style "padding" "0"
            , style "color" "white"
            , style "font-size" "150px" 
            , style "text-shadow" "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000"
            ] 
            [ text "Active Challenges" ]
        , div 
            [ style "display" "flex"
            , style "flex-direction" "column"
            , style "align-items" "flex-start"
            , style "gap" "20px"
            , style "width" "90%"
            ] 
            [ div 
                [ style "display" "flex"
                , style "flex-direction" "column"
                , style "align-items" "flex-start"
                , style "gap" "10px"
                ] 
                [ label 
                    [ style "color" "black"
                    , style "font-size" "50px"
                    ] 
                    [ text "Challenge Name" ]
                , textarea 
                    [ placeholder "Enter challenge name"
                    , value model.challengeName
                    , onInput UpdateChallengeName
                    , style "padding" "10px"
                    , style "font-size" "18px"
                    , style "border" "2px solid black"
                    , style "border-radius" "4px"
                    , rows 5
                    , cols 110
                    ] 
                    []
                ]
            , div 
                [ style "display" "flex"
                , style "flex-direction" "column"
                , style "align-items" "flex-start"
                , style "gap" "10px"
                ] 
                [ label 
                    [ style "color" "black"
                    , style "font-size" "50px"
                    ] 
                    [ text "Problem Statement" ]
                , textarea 
                    [ placeholder "Enter problem statement"
                    , value model.problemStatement
                    , onInput UpdateProblemStatement
                    , style "padding" "10px"
                    , style "font-size" "18px"
                    , style "border" "2px solid black"
                    , style "border-radius" "4px"
                    , rows 20
                    , cols 110
                    ] 
                    []
                ]
            ]
        , div
            [ style "position" "absolute"
            , style "top" "30%"
            , style "right" "5%"  
            , style "width" "20%"  
            , style "display" "flex"
            , style "flex-direction" "column"
            , style "align-items" "flex-end"
            , style "gap" "20px"
            ]
            [ div
                [ style "display" "flex"
                , style "flex-direction" "column"
                , style "align-items" "flex-end"
                ]
                [ label [ style "color" "black", style "font-size" "20px" ] [ text "Start Date and Time" ]
                , input
                    [ type_ "datetime-local"
                    , value model.startDateTime
                    , onInput UpdateStartDateTime
                    , style "padding" "10px"
                    , style "font-size" "20px"
                    , style "border" "2px solid black"
                    , style "border-radius" "4px"
                    , style "width" "100%"
                    ]
                    []
                ]
            , div
                [ style "display" "flex"
                , style "flex-direction" "column"
                , style "align-items" "flex-end"
                ]
                [ label [ style "color" "black", style "font-size" "20px" ] [ text "End Date and Time" ]
                , input
                    [ type_ "datetime-local"
                    , value model.endDateTime
                    , onInput UpdateEndDateTime
                    , style "padding" "10px"
                    , style "font-size" "20px"
                    , style "border" "2px solid black"
                    , style "border-radius" "4px"
                    , style "width" "100%"
                    ]
                    []
                ]
            ]
            , img
            [ src "ducky.png"
            , style "position" "absolute"
            , style "bottom" "20px"
            , style "right" "20px"
            , style "width" "100px"
            , style "height" "auto"
            ] 
            []
            , img
            [ src "ducky.png"
            , style "position" "absolute"
            , style "bottom" "20px"
            , style "right" "130px"
            , style "width" "120px"
            , style "height" "auto"
            ] 
            []
            , img
            [ src "ducky.png"
            , style "position" "absolute"
            , style "bottom" "20px"
            , style "right" "245px"
            , style "width" "180px"
            , style "height" "auto"
            ] 
            []
        ]
