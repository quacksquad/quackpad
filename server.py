from flask import Flask, jsonify
import mysql.connector as mys
from flask_cors import CORS

server = Flask(__name__)
CORS(server)

try:
    db = mys.connect(host = "localhost", user = "sumithra", passwd = "Sumithra@SQL#123", database = "quackpad")


    if db.is_connected():
        print("Connection Succesful:)")

except mys.Error as e:
    print(f"Error: {e}")

cursor = db.cursor()
    

@server.route('/api/qpad_data', methods = ['GET'])

def get_scores():
    cursor.execute("SELECT username, points FROM users;")
    data = cursor.fetchall()

    #print(data)

    result = [{"username": row[0], "points": row[1]} for row in data]
    return jsonify(result)


if __name__ == '__main__':
    server.run(debug=True)
#get_scores()

