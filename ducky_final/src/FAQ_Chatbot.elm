module FAQ_Chatbot exposing (..)

import Html exposing (Html, div, h1, p, ul, li, h2, text, details, summary)
import Html.Attributes exposing (style, id)

type alias FAQ =
    { question : String
    , answer : String
    }

type alias Model =
    { faqs : List FAQ
    , challengeFaqs : List FAQ
    , leaderboardFaqs : List FAQ
    , communityFaqs : List FAQ
    }

init : Model
init =
    { faqs =
        [ { question = "What is QuackPad?", answer = "QuackPad is an online platform where users can solve coding challenges, improve their programming skills, understanding of concepts discussed, and compete with others on leaderboards based on their performance." }
        , { question = "How do I sign up for QuackPad?", answer = "To sign up, click on the Sign Up button on the homepage, fill out the required information, and follow the instructions to create your account." }
        , { question = "Is QuackPad open to everyone?", answer = "We are currently only open to program scholars that we are in a tie up with. However, we look forward to expanding our website to a larger community in the future." }
        ]
    , challengeFaqs =
        [ { question = "What type of coding challenges are available on QuackPad?", answer = "QuackPad offers a variety of coding challenges, including algorithms, data structures, databases, and more based on the concepts the cohort covers. Challenges range from simple to complex levels, providing something for everyone." }
        , { question = "How are coding challenges structured?", answer = "Each challenge comes with a problem statement, input and output specifications, and sample test cases. Users need to write code to solve the problem and pass all test cases to complete the given challenge." }
        , { question = "Can I see solutions to challenges after I submit my own?", answer = "Yes, after you successfully solve a challenge, you may be able to view and learn from other user's solutions if the challenge host enables the view all solutions option." }
        ]
    , leaderboardFaqs =
        [ { question = "How are points calculated for the leaderboards?", answer = "Points are awarded based on the difficulty of the challenge, the efficiency of your code followed by the time taken to solve the challenge. The more efficient and accurate your code, the more points you earn." }
        , { question = "How often are the leaderboards updated?", answer = "Leaderboards are updated in real-time as users submit their solutions and earn points." }
        , { question = "Can I view my ranking on the leaderboard?", answer = "Yes, you may view your ranking on both the cohort leaderboard, as well as see how you stack up against your cohortmates." }
        ]
    , communityFaqs =
        [ { question = "How can I interact with other users on QuackPad?", answer = "You can interact with your fellow cohortmates through comment sections on challenges. The QuackPad team aims to add a discussion forum feature in the future that will enable you to directly interact with your cohort mates." }
        , { question = "Where can I get help if I encounter an issue?", answer = "If you encounter any issues, you can look at the FAQ's and see if it helps solve your problem or contact our support team through the 'Contact Us' page." }
        ]
    }

type Msg
    = NoOp

update : Msg -> Model -> Model
update msg model =
    case msg of
        NoOp ->
            model

view : Model -> Html Msg
view model =
    div [ style "font-family" "Arial, sans-serif"
        , style "margin" "0 auto"
        , style "max-width" "100vw"
        , style "min-height" "100vh"
        , style "padding" "20px"
        , style "background-image" "url('assets/LB_bg.png')"
        , style "background-size" "cover"
        , style "background-repeat" "no-repeat"
        , style "background-position" "center"
        ]
        [ h1 [ style "text-align" "center" ] [ text "Meet Ducky, our AI Chatbot" ]
        , p [ style "text-align" "center" ] [ text "Check out our FAQ's to find answers to your queries. If you can't find an answer then our Ducky, our chatbot is here to help you with all your queries." ]
        , div [ style "display" "flex", style "justify-content" "space-between" ]
            [ div [ id "faq-column", style "width" "60%", style "text-align" "center" ]
                [ h2 [ style "text-align" "center", style "margin-top" "40px" ] [ text "About QuackPad" ]
                , ul [ style "list-style-type" "none", style "padding" "0", style "display" "flex", style "flex-direction" "column", style "align-items" "center" ]
                    (List.map (viewFAQ "yellow") model.faqs)
                , h2 [ style "text-align" "center", style "margin-top" "40px" ] [ text "Our Challenges" ]
                , ul [ style "list-style-type" "none", style "padding" "0", style "display" "flex", style "flex-direction" "column", style "align-items" "center" ]
                    (List.map (viewFAQ "yellow") model.challengeFaqs)
                , h2 [ style "text-align" "center", style "margin-top" "40px" ] [ text "About the Leaderboard" ]
                , ul [ style "list-style-type" "none", style "padding" "0", style "display" "flex", style "flex-direction" "column", style "align-items" "center" ]
                    (List.map (viewFAQ "yellow") model.leaderboardFaqs)
                , h2 [ style "text-align" "center", style "margin-top" "40px" ] [ text "Community and Support" ]
                , ul [ style "list-style-type" "none", style "padding" "0", style "display" "flex", style "flex-direction" "column", style "align-items" "center" ]
                    (List.map (viewFAQ "yellow") model.communityFaqs)
                ]
            , div [ id "aichatbot", style "width" "40%", style "margin-left" "20px" ]
                []
            ]
        ]

viewFAQ : String -> FAQ -> Html Msg
viewFAQ bgColor faq =
    li [ style "margin" "10px 0"
       , style "border" "1px solid #ccc"
       , style "border-radius" "5px"
       , style "box-shadow" "0 2px 5px rgba(0,0,0,0.1)"
       , style "padding" "10px"
       , style "background-color" bgColor
       , style "width" "80%"
       , style "text-align" "left"
       ]
        [ details []
            [ summary [ style "font-weight" "bold", style "cursor" "pointer" ] [ text faq.question ]
            , div [ style "margin-top" "10px" ] [ text faq.answer ]
            ]
        ]
