===============================================================================
                               QuackPad
===============================================================================

QuackPad is a code posting web application designed to foster a conducive
learning environment for users interested in solving coding challenges. The
platform provides a range of features, including user registration and login,
active coding challenges, leaderboards, past challenges, and a chatbot for
assistance. Admins have the capability to grade submissions to ensure accurate
scoring.

===============================================================================
Features
===============================================================================

- User Registration/Login
- Main menu page
- Active Challenges
- Past Challenges
- Leaderboard
- FAQ Page and Chatbot

===============================================================================
Components
===============================================================================

Login and Register Page
-----------------------
The login and register page allows users to create an account or log in to their
existing account. This is the gateway for users to access all other features of
QuackPad.

Active Challenges
-----------------
Active challenges are coding problems that users can participate in. These
challenges are time-bound and contribute to the user's overall score on the
leaderboard.

Past Challenges
---------------
Past challenges provide users with access to previously held coding challenges,
including problem statements, end dates, and a solution to the given problem statement. This feature allows
users to learn from past problems and improve their coding skills.

Leaderboard
-----------
The leaderboard displays the scores of users based on their performance in
active challenges. It fosters a competitive environment and motivates users to
perform better.

FAQ and Chatbot
-----------
A FAQ section allows users to look for answers to the most frequently asked questions.
If these do not satisfy their queries, our AI-powered chatbot Ducky,is available to 
assist users with queries by clicking the small icon on the bottom right of the website.

===============================================================================
Getting Started
===============================================================================

Prerequisites
-------------
- Elm installed
- A SQL database for storing challenge data and user information

Installation
------------
1. Clone the repository:
   ```sh
   git clone https://github.com/your-username/quackpad.git
   cd quackpad

2. How we set up the SQL database:
- Create a database and import the provided SQL schema.
- Update the database connection settings in the project configuration 

3. Install the required dependencies
- install elm

4. Run the project
- elm reactor (command line in terminal) 
- open the index.html page 

Usage
-------------
- Open the application in your web browser.
- Register a new account or log in with an existing account.
- Navigate to the "Active Challenges" section to participate in current coding challenges.
- View your ranking on the leaderboard 
- Explore the "Past Challenges" to review previous problems and a solution to the same.
- Use the FAQ page and chatbot for any assistance needed.



===============================================================================
Contact 
===============================================================================

For any questions or feedback, please contact us through any of the following emails
- pranjalaarai2731@gmail.com
- 2sumithra.suresh@gmail.com
- tanyatrips@gmail.com
