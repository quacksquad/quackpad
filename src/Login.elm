module Login exposing (..)

import Browser.Navigation as Navigation
import Html exposing (Html, div, input, button, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Url exposing (Url)

-- MODEL

type alias Model =
    { username : String
    , password : String
    , loginFailed : Bool
    , isSubmitted : Bool
    , key : Navigation.Key
    }

init : String -> String -> Navigation.Key -> ( Model, Cmd Msg )
init _ _ key =
    ( { username = ""
      , password = ""
      , loginFailed = False
      , isSubmitted = False
      , key = key
      }
    , Cmd.none
    )

-- MESSAGES

type Msg
    = UpdateUsername String
    | UpdatePassword String
    | Submit

-- UPDATE

update : Msg -> Model -> String -> String -> ( Model, Cmd Msg )
update msg model registeredUsername registeredPassword =
    case msg of
        UpdateUsername username ->
            ( { model | username = username }, Cmd.none )

        UpdatePassword password ->
            ( { model | password = password }, Cmd.none )

        Submit ->
            let
                updatedModel = { model | isSubmitted = True }
            in
            if model.username == registeredUsername && model.password == registeredPassword then
                ( { updatedModel | loginFailed = False }, Navigation.pushUrl model.key "/" )
            else
                ( { updatedModel | loginFailed = True }, Cmd.none )

-- VIEW

view : Model -> Html Msg
view model =
    div []
        [ input [ placeholder "Username", value model.username, onInput UpdateUsername, type_ "text" ] []
        , input [ placeholder "Password", value model.password, onInput UpdatePassword, type_ "password" ] []
        , button [ onClick Submit ] [ text "Login" ]
        , if model.loginFailed then
            div [ ] [ text "Login failed. Please try again." ]
          else
            text ""
        ]

-- UTILITY FUNCTION

isLoggedIn : Model -> Bool
isLoggedIn model =
    not model.loginFailed && model.isSubmitted
