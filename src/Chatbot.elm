module Chatbot exposing (..)

import Browser
import Html exposing (Html, div)
import Html.Attributes exposing (style, id)

type alias Model =
    { 
    }

init : Model
init =
    { 
    }

type Msg
    = NoOp

update : Msg -> Model -> Model
update msg model =
    case msg of
        NoOp ->
            model

view : Model -> Html Msg
view model =
    div [ id "aichatbot", style "width" "40%", style "margin-left" "20px" ] []


main : Program () Model Msg
main =
    Browser.sandbox { init = init, update = update, view = view }
