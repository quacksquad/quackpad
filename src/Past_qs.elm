module Past_qs exposing (..)

import Html exposing (Html, div, button, text, a, h1, p, h2, section, pre, code, img)
import Html.Attributes exposing (href, style, src)
import Html.Events exposing (onClick)

type alias Challenge =
    { id : String
    , problemStatement : String
    , solution : String
    , endDate : String
    }

type alias Model =
    { challenges : List Challenge
    , selectedChallenge : Maybe Challenge
    }

init : Model
init =
    { challenges =
        [ { id = "1", problemStatement = "Write a function that takes a string as input and returns the string reversed.", solution = "def reverse_string(s):\n    return s[::-1]", endDate = "2024-07-01" }
        , { id = "2", problemStatement = "Write a function that checks if a given string is a palindrome.", solution = "def is_palindrome(s):\n    return s == s[::-1]", endDate = "2024-07-02" }
        , { id = "3", problemStatement = "Write a function to generate the Fibonacci sequence up to the nth term.", solution = "def fibonacci(n):\n    sequence = [0, 1]\n    while len(sequence) < n:\n        sequence.append(sequence[-1] + sequence[-2])\n    return sequence[:n]", endDate = "2024-07-03" }
        , { id = "4", problemStatement = "Write a function that counts the number of vowels in a given string.", solution = "def count_vowels(s):\n    vowels = 'aeiou'\n    return sum(1 for char in s if char in vowels)", endDate = "2024-07-04" }
        , { id = "5", problemStatement = "Write a function that returns the sum of all elements in a list.", solution = "def sum_list(lst):\n    return sum(lst)", endDate = "2024-07-05" }
        , { id = "6", problemStatement = "Write a function that finds the largest number in a list of integers.", solution = "def find_largest(lst):\n    return max(lst)", endDate = "2024-07-06" }
        , { id = "7", problemStatement = "Write a function that removes duplicates from a list and returns the unique elements.", solution = "def remove_duplicates(lst):\n    return list(set(lst))", endDate = "2024-07-07" }
        , { id = "8", problemStatement = "Write a function that sorts a list of tuples based on the second element of each tuple.", solution = "def sort_by_second(tuples):\n    return sorted(tuples, key=lambda x: x[1])", endDate = "2024-07-08" }
        , { id = "9", problemStatement = "Write a function to calculate the factorial of a given number using recursion.", solution = "def factorial(n):\n    if n == 0:\n        return 1\n    return n * factorial(n - 1)", endDate = "2024-07-09" }
        , { id = "10", problemStatement = "Write a function that finds common elements between two lists.", solution = "def common_elements(lst1, lst2):\n    return list(set(lst1) & set(lst2))", endDate = "2024-07-10" }
        , { id = "11", problemStatement = "Write a function that performs a binary search on a sorted list and returns the index of the target element.", solution = "def binary_search(lst, target):\n    low, high = 0, len(lst) - 1\n    while low <= high:\n        mid = (low + high) // 2\n        if lst[mid] == target:\n            return mid\n        elif lst[mid] < target:\n            low = mid + 1\n        else:\n            high = mid - 1\n    return -1", endDate = "2024-07-11" }
        , { id = "12", problemStatement = "Write a function that merges two sorted lists into a single sorted list.", solution = "def merge_sorted_lists(lst1, lst2):\n    merged_list = []\n    i, j = 0, 0\n    while i < len(lst1) and j < len(lst2):\n        if lst1[i] < lst2[j]:\n            merged_list.append(lst1[i])\n            i += 1\n        else:\n            merged_list.append(lst2[j])\n            j += 1\n    merged_list.extend(lst1[i:])\n    merged_list.extend(lst2[j:])\n    return merged_list", endDate = "2024-07-12" }
        , { id = "13", problemStatement = "Write a function that finds the length of the longest substring without repeating characters.", solution = "def longest_substring(s):\n    char_map = {}\n    left, max_length = 0, 0\n    for right, char in enumerate(s):\n        if char in char_map and char_map[char] >= left:\n            left = char_map[char] + 1\n        char_map[char] = right\n        max_length = max(max_length, right - left + 1)\n    return max_length", endDate = "2024-07-13" }
        , { id = "14", problemStatement = "Write a function to find the kth largest element in an unsorted array.", solution = "import heapq\n\ndef kth_largest(nums, k):\n    return heapq.nlargest(k, nums)[-1]", endDate = "2024-07-14" }
        , { id = "15", problemStatement = "Write a function to rotate an n x n matrix 90 degrees clockwise.", solution = "def rotate_matrix(matrix):\n    return [list(row) for row in zip(*matrix[::-1])]", endDate = "2024-07-15" }
        ]
    , selectedChallenge = Nothing
    }

type Msg
    = SelectChallenge String
    | DeselectChallenge

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SelectChallenge id ->
            let
                selected =
                    List.filter (\c -> c.id == id) model.challenges
            in
            case selected of
                [challenge] ->
                    ( { model | selectedChallenge = Just challenge }, Cmd.none )
                _ ->
                    ( model, Cmd.none )

        DeselectChallenge ->
            ( { model | selectedChallenge = Nothing }, Cmd.none )

view : Model -> Html Msg
view model =
    div [ style "background-image" "url('assets/LB_bg.png')"
        , style "background-size" "cover"
        , style "background-position" "center"
        , style "background-attachment" "fixed"
        , style "width" "100vw"
        , style "height" "100vh"
        , style "margin" "0"
        , style "padding" "0"
        ]
        [ case model.selectedChallenge of
            Just challenge ->
                div [ style "max-width" "800px"
                    , style "margin" "20px auto"
                    , style "padding" "20px"
                    , style "background-color" "white"
                    , style "border-radius" "8px"
                    , style "box-shadow" "0 0 10px rgba(0, 0, 0, 0.1)"
                    , style "text-align" "center"
                    ]
                    [ h1 [] [ text "Challenge Details" ]
                    , p [] [ text ("ID: " ++ challenge.id) ]
                    , p [ style "font-size" "1.2em" ] [ text ("Problem Statement: " ++ challenge.problemStatement) ]
                    , p [] [ text ("End Date: " ++ challenge.endDate) ]
                    , pre [ style "background-color" "#f5f5f5"
                          , style "padding" "15px"
                          , style "border-radius" "4px"
                          , style "overflow-x" "auto"
                          , style "font-family" "monospace"
                          , style "white-space" "pre-wrap"
                          , style "word-wrap" "break-word"
                          ]
                        [ code [ style "color" "#333", style "font-size" "1em" ] [ text challenge.solution ] ]
                    , button [ onClick DeselectChallenge
                             , style "padding" "10px"
                             , style "border" "1px solid #ddd"
                             , style "border-radius" "4px"
                             , style "background-color" "#ffeb3b"
                             , style "color" "#333"
                             , style "cursor" "pointer"
                             ] [ text "Back to List" ]
                    ]

            Nothing ->
                div [ style "text-align" "center" ]
                    [ h1 [ style "color" "black" ] [ text "Past Challenges" ]
                    , section [ style "display" "flex"
                              , style "flex-wrap" "wrap"
                              , style "justify-content" "center"
                              , style "gap" "20px"
                              , style "padding" "20px"
                              , style "background-image" "url('assets/LB_bg.png')"
                              , style "background-size" "cover"
                              , style "background-position" "center"
                              , style "background-attachment" "fixed"
                              ]
                        (List.map challengeBox model.challenges)
                    ]
        ]

challengeBox : Challenge -> Html Msg
challengeBox challenge =
    a [ href "#", onClick (SelectChallenge challenge.id)
      , style "display" "block"
      , style "width" "300px"
      , style "margin" "10px"
      , style "padding" "15px"
      , style "border" "1px solid #ddd"
      , style "border-radius" "4px"
      , style "background" "linear-gradient(135deg, #fff9c4, #ffeb3b)"
      , style "text-decoration" "none"
      , style "color" "#333"
      , style "text-align" "left"
      ]
        [ div [ style "display" "flex", style "align-items" "center" ]
            [ img [ src "assets/ducky.png", style "width" "30px", style "height" "30px", style "margin-right" "10px" ] []
            , div []
                [ h2 [] [ text ("Challenge ID: " ++ challenge.id) ]
                , p [ style "font-size" "1.1em" ] [ text ("Problem Statement: " ++ challenge.problemStatement) ]
                , p [] [ text ("End Date: " ++ challenge.endDate) ]
                ]
            ]
        ]

