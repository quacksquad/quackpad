module Register exposing (..)

import Browser.Navigation as Navigation
import Html exposing (Html, div, input, button, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Url exposing (Url)

-- MODEL

type alias Model =
    { username : String
    , password : String
    , key : Navigation.Key
    }

init : String -> String -> Navigation.Key -> ( Model, Cmd Msg )
init username password key =
    ( { username = username
      , password = password
      , key = key
      }
    , Cmd.none
    )

-- MESSAGES

type Msg
    = UpdateUsername String
    | UpdatePassword String
    | Register

-- UPDATE

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateUsername username ->
            ( { model | username = username }, Cmd.none )

        UpdatePassword password ->
            ( { model | password = password }, Cmd.none )

        Register ->
            ( model, Cmd.none )

-- VIEW

view : Model -> Html Msg
view model =
    div []
        [ input [ placeholder "Username", value model.username, onInput UpdateUsername, type_ "text" ] []
        , input [ placeholder "Password", value model.password, onInput UpdatePassword, type_ "password" ] []
        , button [ onClick Register ] [ text "Register" ]
        ]
