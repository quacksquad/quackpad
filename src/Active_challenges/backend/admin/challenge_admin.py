import mysql.connector
from mysql.connector import errorcode
from datetime import datetime

conn = mysql.connector.connect(
    user='root',
    password='SK6ksh6y6@06',
    host='localhost',
    database='QUACKPAD')
cursor = conn.cursor()

cid = input("Enter challenge id: ")
cname = input("Enter challenge name: ")
prblm = input("Enter problem statement: ")
stop_time = datetime.strptime(input("Enter the ending time for the challenge (YYYY-MM-DD HH:MM:SS): "), '%Y-%m-%d %H:%M:%S')
start_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

insert_query = '''insert into challenges (CID, CNAME, PROBLEM, START_TIME, STOP_TIME)
        VALUES (%s, %s, %s, %s, %s)'''
cursor.execute(insert_query, (cid, cname, prblm, start_time, stop_time))

create_table_query = f'''create table {cid} (
            sno int auto_increment primary key,
            name varchar(50),
            time_submitted timestamp default current_timestamp,
            submission varchar(50),
            score int default 0)'''

cursor.execute(create_table_query)
    
cursor.close()
conn.close()
