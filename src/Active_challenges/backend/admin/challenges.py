import mysql.connector
from mysql.connector import errorcode
from datetime import datetime

conn = mysql.connector.connect(user='root', password='SK6ksh6y6@06', host='localhost')
cursor = conn.cursor()

cursor.execute("create database QUACKPAD")

cursor.execute("use QUACKPAD")

cursor.execute('''create table challenges 
               (CID char(4),
               CNAME varchar(30),
               PROBLEM varchar(255),
               START_TIME datetime,
               STOP_TIME datetime)''')

cursor.close()
conn.close()

