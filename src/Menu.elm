module Menu exposing (..)

import Browser
import Browser.Navigation as Navigation
import Html exposing (Html, button, div, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Url exposing (Url)
import Leaderboard exposing (..)
import Past_qs exposing (..)
import Login exposing (view, Msg, init, update, isLoggedIn)
import Register exposing (view, Msg, init, update)
import FAQ_Chatbot exposing (view, init)
import Active_challenges.Frontend.Admin_active as Admin_active exposing (..)

-- MODEL

type alias Model =
    { key : Navigation.Key
    , currentPage : Page
    , pastQsModel : Past_qs.Model
    , loginModel : Login.Model
    , registerModel : Register.Model
    , leaderboardModel : Leaderboard.Model
    , isLoggedIn : Bool
    , registeredUsername : String
    , registeredPassword : String
    , faqModel : FAQ_Chatbot.Model
    , adminActiveModel : Admin_active.Model
    }

type Page
    = Home
    | ActiveChallenges
    | PastChallenges
    | Leaderboard
    | LoginPage
    | RegisterPage
    | FAQPage

init : () -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init _ url key =
    let
        (registerModel, registerCmd) = Register.init "" "" key
        (loginModel, loginCmd) = Login.init "" "" key
    in
    ( { key = key
      , currentPage = RegisterPage
      , pastQsModel = Past_qs.init
      , loginModel = loginModel
      , registerModel = registerModel
      , leaderboardModel = Leaderboard.initialModel
      , isLoggedIn = False
      , registeredUsername = ""
      , registeredPassword = ""
      , faqModel = FAQ_Chatbot.init
      , adminActiveModel = Admin_active.init
      }
    , Cmd.batch [ Cmd.map RegisterMsg registerCmd, Cmd.map LoginMsg loginCmd ]
    )

-- UPDATE

type Msg
    = NavigateTo Page
    | UrlChanged Url
    | HandleUrlRequest Browser.UrlRequest
    | PastQsMsg Past_qs.Msg
    | LoginMsg Login.Msg
    | RegisterMsg Register.Msg
    | FAQMsg FAQ_Chatbot.Msg
    | LeaderboardMsg Leaderboard.Msg
    | AdminActiveMsg Admin_active.Msg

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NavigateTo page ->
            let
                cmds =
                    case page of
                        Leaderboard ->
                            [ Cmd.map LeaderboardMsg Leaderboard.fetchUsers ]
                        _ ->
                            []
            in
            ( { model | currentPage = page }
            , Cmd.batch (Navigation.pushUrl model.key (toUrl page) :: cmds)
            )

        UrlChanged url ->
            ( { model | currentPage = fromUrl url }, Cmd.none )

        HandleUrlRequest urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Navigation.pushUrl model.key (Url.toString url) )
                Browser.External href ->
                    ( model, Navigation.load href )

        PastQsMsg pastQsMsg ->
            let
                (updatedPastQsModel, pastQsCmd) = Past_qs.update pastQsMsg model.pastQsModel
            in
            ( { model | pastQsModel = updatedPastQsModel }
            , Cmd.map PastQsMsg pastQsCmd
            )

        LoginMsg loginMsg ->
            let
                (updatedLoginModel, loginCmd) = Login.update loginMsg model.loginModel model.registeredUsername model.registeredPassword
                newIsLoggedIn = Login.isLoggedIn updatedLoginModel
            in
            ( { model | loginModel = updatedLoginModel
                      , isLoggedIn = newIsLoggedIn
                      , currentPage = if newIsLoggedIn then Home else LoginPage
              }
            , Cmd.map LoginMsg loginCmd
            )

        RegisterMsg registerMsg ->
            let
                (updatedRegisterModel, registerCmd) = Register.update registerMsg model.registerModel
                (username, password) = (updatedRegisterModel.username, updatedRegisterModel.password)
            in
            case registerMsg of
                Register.Register ->
                    ( { model | registeredUsername = username, registeredPassword = password, currentPage = LoginPage }
                    , Cmd.batch [ Navigation.pushUrl model.key "/login", Cmd.map RegisterMsg registerCmd ]
                    )
                _ ->
                    ( { model | registerModel = updatedRegisterModel }
                    , Cmd.map RegisterMsg registerCmd
                    )
        LeaderboardMsg leaderboardMsg ->
            let
                (updatedLeaderboardModel, leaderboardCmd) = Leaderboard.update leaderboardMsg model.leaderboardModel
            in
            ( { model| leaderboardModel = updatedLeaderboardModel }
            , Cmd.map LeaderboardMsg leaderboardCmd
            )

        FAQMsg faqMsg ->
            let
                updatedFaqModel = FAQ_Chatbot.update faqMsg model.faqModel
            in
            ( { model | faqModel = updatedFaqModel }
            , Cmd.none
            )

        AdminActiveMsg adminActiveMsg ->
            let
                updatedAdminActiveModel = Admin_active.update adminActiveMsg model.adminActiveModel
            in
            ( { model | adminActiveModel = updatedAdminActiveModel }
            , Cmd.none
            )

-- VIEW

view : Model -> Browser.Document Msg
view model =
    { title = "Menu"
    , body =
        [ div [ class "container" ]
            [ case model.currentPage of
                RegisterPage ->
                    Html.map RegisterMsg (Register.view model.registerModel)

                LoginPage ->
                    Html.map LoginMsg (Login.view model.loginModel)

                Home ->
                    div []
                        [ div [ class "page-title" ] [ text "Menu" ]
                        , button [ onClick (NavigateTo ActiveChallenges), class "menu-button" ] [ text "Active Challenges" ]
                        , button [ onClick (NavigateTo PastChallenges), class "menu-button" ] [ text "Past Challenges" ]
                        , button [ onClick (NavigateTo Leaderboard), class "menu-button" ] [ text "Leaderboard" ]
                        , button [ onClick (NavigateTo FAQPage), class "menu-button" ] [ text "FAQ" ]
                        ]

                ActiveChallenges ->
                    Html.map AdminActiveMsg (Admin_active.view model.adminActiveModel)

                PastChallenges ->
                    Html.map PastQsMsg (Past_qs.view model.pastQsModel)

                Leaderboard ->
                    Html.map LeaderboardMsg (Leaderboard.view model.leaderboardModel)

                FAQPage ->
                    Html.map FAQMsg (FAQ_Chatbot.view model.faqModel)
                    
                                
            ]
        ]
    }

-- URL PARSING

toUrl : Page -> String
toUrl page =
    case page of
        Home ->
            "/"

        ActiveChallenges ->
            "/active-challenges"

        PastChallenges ->
            "/past-challenges"

        Leaderboard ->
            "/leaderboard"

        LoginPage ->
            "/login"

        RegisterPage ->
            "/register"

        FAQPage ->
            "/faq"

fromUrl : Url -> Page
fromUrl url =
    case url.path of
        "/active-challenges" ->
            ActiveChallenges

        "/past-challenges" ->
            PastChallenges

        "/leaderboard" ->
            Leaderboard

        "/login" ->
            LoginPage

        "/register" ->
            RegisterPage

        "/faq" ->
            FAQPage

        _ ->
            Home

-- MAIN

main =
    Browser.application
        { init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view
        , onUrlRequest = HandleUrlRequest
        , onUrlChange = UrlChanged
        }
