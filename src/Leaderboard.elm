module Leaderboard exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (style, src)
import Http
import Json.Decode exposing (Decoder, field, int, list, string)
import Debug -- For debugging

-- Model

type alias User =
    { name : String
    , points : Int
    }

type alias Model =
    { users : List User
    , error : Maybe String
    }

initialModel : Model
initialModel =
    { users = []
    , error = Nothing
    }

logIn : String
logIn =
    "user2"


-- Decoders

userDecoder : Decoder User
userDecoder =
    Json.Decode.map2 User
        (field "username" string)
        (field "points" int)

usersDecoder : Decoder (List User)
usersDecoder =
    list userDecoder


-- Messages

type Msg
    = FetchData
    | FetchDataResult (Result Http.Error (List User))


-- Update

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        FetchData ->
            ( model, fetchUsers )

        FetchDataResult (Ok users) ->
            -- Log the fetched users data to the console
            let
                loggedUsers = Debug.log "Fetched Users" users
            in
            ( { model | users = loggedUsers, error = Nothing }, Cmd.none )

        FetchDataResult (Err error) ->
            -- Log the error message to the console
            let
                loggedError = Debug.log "Fetch Error" error
            in
            ( { model | error = Just "Failed to fetch data" }, Cmd.none )


fetchUsers : Cmd Msg
fetchUsers =
    Http.get
        { url = "http://127.0.0.1:5000/api/qpad_data"
        , expect = Http.expectJson FetchDataResult usersDecoder
        }


-- View

userBar : Int -> User -> Html msg
userBar index user =
    let
        barColor =
            case index of
                0 -> "#e6b400"
                1 -> "#e6cc00"
                2 -> "#e5de00"
                3 -> "#e8e337"
                4 -> "#ece75f"
                _ -> "#f1ee8e"

        barStyles =
            [ style "display" "flex"
            , style "align-items" "center"
            , style "margin" "5px 0"
            , style "padding" "10px"
            , style "background-color" barColor
            , style "width" "100%"
            , style "height" "30px"
            , style "border-radius" "5px"
            , style "border" "1px solid black"
            ]

        imageSrc = "/assets/ducky.png"
        imageStyle =
            [ style "width" "30px"
            , style "margin-left" "auto"
            ]

        pointsStyle =
            [ style "margin-left" "10px"
            , style "margin-right" "10px"
            ]
    in
    div barStyles
        [ text ("(" ++ String.fromInt (index + 1) ++ ")   " ++ user.name)
        , span pointsStyle [ text (String.fromInt user.points) ]
        , img ([ src imageSrc ] ++ imageStyle) []
        ]

getUserPosition : String -> List User -> Maybe Int
getUserPosition userName users =
    users
        |> List.indexedMap Tuple.pair
        |> List.filter (\(_, user) -> user.name == userName)
        |> List.head
        |> Maybe.map Tuple.first

-- Function to calculate the ranges for bar graph
calculateRanges : List User -> List (Int, (Int, Int))
calculateRanges users =
    let
        pointsList = List.map .points users
        maxPoints = Maybe.withDefault 0 (List.maximum pointsList)
        minPoints = Maybe.withDefault 0 (List.minimum pointsList)
        difference = maxPoints - minPoints
        remainder = Basics.remainderBy 6 difference
        adjustedMaxPoints =
            if remainder == 0 then
                maxPoints
            else
                maxPoints + (6 - remainder)
        range = (adjustedMaxPoints - minPoints) // 6
        ranges = List.range 0 5 
        _ = Debug.log "Max points" adjustedMaxPoints
    in
    List.map (\i -> 
        ( List.length (
            List.filter (\user -> 
                user.points >= minPoints + i * range 
                && user.points < minPoints + (i + 1) * range
            ) users
        ), 
        (minPoints + i * range, minPoints + (i + 1) * range)
        )
    ) ranges

barGraph : List (Int, (Int, Int)) -> Html msg
barGraph ranges =
    div [ style "display" "flex", style "align-items" "flex-end", style "width" "100%", style "height" "200px", style "margin-top" "20px", style "padding" "10px", style "margin-left" "auto", style "margin-right" "auto", style "background-color" "transparent" ]
        (List.indexedMap (\index (count, (rangeStart, rangeEnd)) ->
            div [ style "width" "14%", style "margin" "0 6px", style "text-align" "center" ]
                [ div [ style "width" "100%", style "height" (String.fromInt (count * 5) ++ "px"), style "background-color" "#191a48", style "color" "white", style "display" "flex", style "align-items" "flex-end", style "justify-content" "center" ]
                    [ text (String.fromInt count) ]
                , div [] [ text (String.fromInt rangeStart ++ " - " ++ String.fromInt rangeEnd) ]
                ]
            ) ranges)

view : Model -> Html msg
view model =
    let
        sortedUsers = List.sortBy .points model.users |> List.reverse
        topSixUsers = List.take 6 sortedUsers
        userPosition = getUserPosition logIn sortedUsers
        ranges = calculateRanges model.users
        userRankText =
            case userPosition of
                Just pos -> "Your Ducky Rank : " ++ String.fromInt (pos + 1)
                Nothing -> "Your Ducky Rank : -"
    in
    div [ style "display" "flex"
        , style "flex-direction" "column"
        , style "align-items" "flex-start"
        , style "width" "100%"
        , style "height" "100vh"
        , style "background-image" "url('/assets/LB_bg.png')"
        , style "background-size" "cover"
        , style "background-position" "center"
        , style "position" "absolute"
        , style "top" "0"
        , style "left" "0"
        ]
        [ h1 [ style "text-align" "center"
        , style "width" "100%"
        , style "position" "absolute"
        , style "top" "20px"
        , style "font-family" "'Boston Traffic', sans-serif"
        , style "color" "#191a48"
        , style "font-weight" "bold"
        , style "font-size" "50px" 
        ]
         [ text "LEADERBOARD" ]
        , div [ style "margin-left" "50px", style "width" "50%", style "margin-top" "150px" ]
          (List.indexedMap userBar topSixUsers)
        , div [ style "position" "absolute", style "top" "150px", style "right" "50px", style "width" "30%", style "padding" "10px", style "border-radius" "5px", style "background-color" "transparent" ]
          [ div [ style "display" "flex", style "justify-content" "flex-start", style "width" "100%", style "margin-bottom" "10px" ]
              [ h2 [ style "text-align" "left", style "font-family" "'Boston Traffic', sans-serif", style "color" "#191a48", style "font-weight" "bold", style "font-size" "30px" ] [ text userRankText ] ]
          , barGraph ranges
          ]
        ]



